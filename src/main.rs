// SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

mod handlers;
mod response;
mod util;

use std::{sync::Arc, time::Duration};

use axum::{routing::get, Router};
use clap::{crate_authors, crate_version, Parser};
use listenfd::ListenFd;
use log::info;
use moka::future::Cache;
use reqwest::Client;
use url::Url;

#[derive(Parser, Debug)]
#[command(author = crate_authors!(), version = crate_version!(), about = "Combine multiple DDraceNetwork skin services into one", long_about = None)]
pub struct Opts {
    // Listen address
    #[arg(short, long, env, default_value = "0.0.0.0:3000")]
    listen_address: String,

    // Publicly accessible URL. Visible on landing page
    #[arg(short = 'u', long, env, default_value = "http://skinprox.invalid")]
    public_url: String,

    // Timeout duration (seconds)
    #[arg(short = 't', long, env, default_value_t = 2u64)]
    timeout: u64,

    // List of upstream skin providers
    #[arg(short, long, env, required = true, value_delimiter = ',')]
    providers: Vec<Url>,
}

#[derive(Clone)]
pub struct AppState {
    opts: Arc<Opts>,
    client: Client,
    provider_cache: Cache<String, Result<String, response::Error>>,
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let opts = Arc::new(Opts::parse());
    let timeout = opts.timeout;

    let state = AppState {
        opts,
        client: Client::builder()
            .user_agent("codeberg.org/Scrumplex/skinprox (contact@scrumplex.net)")
            .timeout(Duration::from_secs(timeout))
            .build()
            .expect("Couldn't build reqwest client"),
        provider_cache: Cache::builder()
            .name("provider_cache")
            .max_capacity(10_000)
            .time_to_live(Duration::from_secs(600))
            .time_to_idle(Duration::from_secs(60))
            .build(),
    };

    let app = Router::new()
        .route("/", get(handlers::index))
        .route("/skin/:name", get(handlers::proxy_skin))
        .fallback(handlers::fallback)
        .with_state(state.clone());

    let mut listenfd = ListenFd::from_env();

    let listener = if let Some(listener) = listenfd.take_tcp_listener(0).unwrap() {
        tokio::net::TcpListener::from_std(listener).unwrap()
    } else {
        tokio::net::TcpListener::bind(&state.opts.listen_address)
            .await
            .unwrap()
    };

    info!("listening on {}", listener.local_addr().unwrap());

    axum::serve(listener, app).await.unwrap();
}

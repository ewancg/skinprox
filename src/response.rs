use axum::{http::StatusCode, response::IntoResponse};

#[derive(Debug, Clone)]
pub enum Error {
    BadSkinName,
    NotFound,
    SkinNotFound,
    ProviderError,
    TimedOut,
    Unknown,
}

impl IntoResponse for Error {
    fn into_response(self) -> axum::response::Response {
        match self {
            Self::BadSkinName => {
                (StatusCode::BAD_REQUEST, "Requested skin is invalid").into_response()
            }
            Self::NotFound => (StatusCode::NOT_FOUND, "Not found").into_response(),
            Self::SkinNotFound => (StatusCode::NOT_FOUND, "Skin not found").into_response(),
            Self::ProviderError => {
                (StatusCode::BAD_GATEWAY, "Error from upstream provider").into_response()
            }
            Self::TimedOut => (StatusCode::REQUEST_TIMEOUT, "Request timed out").into_response(),
            Self::Unknown => (StatusCode::INTERNAL_SERVER_ERROR, "Unknown error").into_response(),
        }
    }
}

# SPDX-FileCopyrightText: 2023 Sefa Eyeoglu <contact@scrumplex.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  perSystem = {
    config,
    lib,
    pkgs,
    self',
    ...
  }: {
    pre-commit.settings = {
      excludes = [
        "Cargo.lock"
      ];
      hooks = {
        alejandra.enable = true;
        rustfmt.enable = true;
        clippy.enable = true;
        prettier = {
          enable = true;
          excludes = ["flake.lock"];
        };
      };
    };
    devShells.default = pkgs.mkShell {
      shellHook = ''
        ${config.pre-commit.installationScript}
      '';

      inputsFrom = [self'.packages.default];
      packages = with pkgs; [reuse systemfd clippy];
    };
    formatter = pkgs.alejandra;
  };
}
